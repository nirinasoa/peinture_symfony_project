/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import { Tooltip, Toast, Popover } from 'bootstrap';

import './js/jquery.min.js';
import './js/bootstrap.min.js';
import './js/aos.js';
import './js/owl.carousel.min.js';
import './js/smoothscroll.js';
import './js/custom.js';
import './js/main.js';


// start the Stimulus application
import './bootstrap';