# PEINTURE_SYMFONY_PROJECT

PEINTURE_SYMFONY_PROJECT est un site internet présentant des peintures

## Environnement de développement

### Pré-requis

* PHP 7.3.27
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-requid (sauf Docker er Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```
#### Configuration de l'environnement
* Pour obtenir la barre de debug lors de l'affichage du site 
=> composer require symfony/profiler-pack

* Avant de créer des utilisateurs par exemple, il faut lancer cette commande
=> composer require security
=> composer require orm (Pour pouvoir utiliser la base de données)

* Pour créer un user, on a utilisé la commande
=>php bin/console make:user au lieu de symfony

* Pour installer le phpunit, lancer cette commande:
=> composer require phpunit/phpunit

* Pour installer fixtures, lancer cette commande: 
=> composer require --dev orm-fixtures

* Pour installer Faker PHP: qui génère des fausses données 
=> composer require fakerphp/faker

* La commnde pour jouer des fixtures 
=>symfony console doctrine:fixtures:load

* Pour découper des longues phrases et à remplacer par ...
=> 1 composer require twig/string-extra
   2 composer require twig/extra-bundle

* Lors d'une modification dans css,
=> il faut utiliser: npm run build

* Pour la pagination, 
=> composer require knplabs/knp-paginator-bundle

* Pour éviter l'erreur de autowire sur paramètres, lancer cette commande:
=> composer require sensio/framework-extra-bundle


###### Lancer l'environnement de développement

```bash
 composer install
 npm install
 npm run build
 docker-compose up -d
 symfony serve -d
```
###### Ajouter des données de tests
```bash
 symfony console doctrine:fixtures:load
```
######  Lancer des tests
```bash
php vendor/bin/phpunit --testdox
```



